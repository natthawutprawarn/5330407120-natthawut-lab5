<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <body>
    <h1>Twitter @
	<a href="{rss/channel/link}">
		<xsl:value-of select="rss/channel/link"/>
        </a>
    </h1>
    <table border="1">
      <tr>
        <th>Title</th>
        <th>Publication Date</th>
      </tr>
      <xsl:for-each select="rss/channel/item">
      <tr>
        <td>
		<a href="{link}">
			<xsl:value-of select="title" />
		</a>
	</td>
        <td><xsl:value-of select="pubDate" /></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>