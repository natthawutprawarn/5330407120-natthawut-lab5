<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2">
<xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <body>
                <h1>
                    <img src="{kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href}"/>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
		</h1>
		<p> List of hotels
                    <ul>
                        <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">                            
                            <li>
                                <xsl:value-of select="kml:name"/>
                                    <ul>
                                        <li>
                                            <xsl:variable name="url">
                                                <xsl:value-of select="substring-before(substring-after(kml:description,'&lt;a href=&quot;'),'&quot;&gt;')"/>
                                            </xsl:variable>
                                            <a href="{$url}"><xsl:value-of select="$url"/></a>
					</li>
					<li>
                                            Coordinates: <xsl:value-of select="kml:Point/kml:coordinates"/>
                                        </li>
                                    </ul>
                            </li>                            
                        </xsl:for-each>
                    </ul>
                </p>    
		</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
